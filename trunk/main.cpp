// dow(u)/dow(t) + dow(f(u))/dow(x) = 0, a*f(u) = sin(x) with a = 1

#include <iostream>
#include <fstream>
#include <cmath>
#include <chrono>
#include <vector>
#include <array>
#include <algorithm>
#include <functional>
//#include <mpi.h>
using namespace std;

//declare variables global
 double L = 2*M_PI, Ncell = 10., dx = L/Ncell ;
 double N = 3; // degree of polynomial
 double dt = 0.025, t = 0, t_final = 20; // physical time
//int nt = static_cast<int> (round(t_final/dt)); // number of time steps.
int nt = 100;
 double a = 1;

inline double initFunction(double x) // initial function
{
  return sin(x);
}

inline double dLagrange(int k, int N, double x) // get derivatives of lagrange polynomial
{
  iszero(k < N);
  static std::vector<double> xi = {-1.,0.,1.};
  double value = 0;
  for(int term = 0; term < N; term++) {
    if(term == k) continue;
    double valueTerm = 1./(xi[k] - xi[term]);
    
    for(int i = 0; i<N; i++) {
      if(i == k) continue;
      if(i == term) continue;
      valueTerm *= (x - xi[i])/(xi[k] - xi[i]); }
    value += valueTerm;
  }
  return value;
}

//template <typename T>
std::vector<std::array<double,3>> adding(const std::vector<std::array<double,3>>& a, const std::vector<std::array<double,3>>& b) // adding vectors
{
  //assert(a.size() == b.size());
  std::vector<std::array<double,3>> result1(a.size());
  //std::transform(a.begin(), a.end(), b.begin(),result.begin(),std::plus<T>());
  for(int i=0; i<Ncell; i++)
    for(int j=0; j<3; j++) {
      result1[i][j] = a[i][j] + b[i][j] ;
    }
  return result1;
}

//template <typename T>
std::vector<std::array<double,3>> multiplying(const double& c, const std::vector<std::array<double,3>>& d) // multiplying vector by a scalar
{
  //assert(a.size() == b.size());
  std::vector<std::array<double,3>> result2(d.size());
  //std::transform(d.begin(), d.end(), c,result.begin(),std::multiplies<T>());
  for(int i=0; i<Ncell; i++)
    for(int j=0; j<3; j++) {
      result2[i][j] = c * d[i][j] ;
    }
  return result2;
}

double representation(double x, std::array<double,3> values) // representation of solution profile inside cells
{
  return 0.5*x*(x-1)*values[0] + (1-x*x)*values[1] + 0.5*x*(x+1)*values[2];
}

// Core of the solution- discretised equations with b.c and fluxes
std::vector<std::array<double,3>> RHS(const std::vector<std::array<double,3>> &cellVec) {
  
  std::vector<std::array<double,3>> rhs(Ncell);
  
  std::vector<std::array<double,3>> dLcellVec0(Ncell);
  std::vector<std::array<double,3>> dLcellVec1(Ncell);
  std::vector<std::array<double,3>> dLcellVec2(Ncell);
  
  for(int i=0; i<Ncell; i++) {
    // for(int j=0; j<3; j++) {
    //    dLcellVec0[i][j] = (2./dx)* a*cellVec[i][j] * dLagrange( 0, 3, -1);
    //    dLcellVec1[i][j] = (2./dx)* a*cellVec[i][j] * dLagrange( 1, 3, 0);
    //    dLcellVec2[i][j] = (2./dx)* a*cellVec[i][j] * dLagrange( 2, 3, 1);
    // }

    dLcellVec0[i][0] = a*cellVec[i][0] * dLagrange( 0, 3, -1);
    dLcellVec0[i][1] = a*cellVec[i][1] * dLagrange( 0, 3, 0);
    dLcellVec0[i][2] = a*cellVec[i][2] * dLagrange( 0, 3, 1);

    dLcellVec1[i][0] = a*cellVec[i][0] * dLagrange( 1, 3, -1);
    dLcellVec1[i][1] = a*cellVec[i][1] * dLagrange( 1, 3, 0);
    dLcellVec1[i][2] = a*cellVec[i][2] * dLagrange( 1, 3, 1);

    dLcellVec2[i][0] = a*cellVec[i][0] * dLagrange( 2, 3, -1);
    dLcellVec2[i][1] = a*cellVec[i][1] * dLagrange( 2, 3, 0);
    dLcellVec2[i][2] = a*cellVec[i][2] * dLagrange( 2, 3, 1);
  }
  
  std::vector<double> fluxVecL(Ncell);
  std::vector<double> fluxVecR(Ncell);
  for(int i=1; i<Ncell-1; i++) {
    fluxVecL[i] = a*cellVec[i-1][2];
    fluxVecR[i] = a*cellVec[i][2];
  }
  
  fluxVecR[0] = a*cellVec[0][2];
  fluxVecL[Ncell-1] = a*cellVec[Ncell-2][2];
  
  // ----  computation of numerical flux at the BORDER of the domain.
  
  fluxVecR[Ncell-1] = a*cellVec[Ncell-1][2];
  fluxVecL[0] = fluxVecR[Ncell-1]; //a*cellVec[Ncell-1][2];

  for(int i=0; i<Ncell; i++) {
    rhs[i][0] = 2./dx*3.0 * ( dLcellVec0[i][0]* 1./3. + dLcellVec0[i][1]* 4./3. + dLcellVec0[i][2]* 1./3. + fluxVecL[i]);
    rhs[i][1] = 2./dx*3./4. * ( dLcellVec1[i][0]* 1./3. + dLcellVec1[i][1]* 4./3. + dLcellVec1[i][2]* 1./3.);
    rhs[i][2] = 2./dx*3.0 * ( dLcellVec2[i][0]* 1./3. + dLcellVec2[i][1]* 4./3. + dLcellVec2[i][2]* 1./3. - fluxVecR[i]);
  }
  
  return rhs;
}

int main(){
 std::vector<std::array<double,3>> cellVec(Ncell); // 1 cell/element of vector **1 cell = 3 nodes,end nodes inclusive**
 std::vector<std::array<double,3>> xVec(Ncell); // vector for x coordinates
 
 for(int i=0; i<Ncell; i++) { //filling the vector of arrays with x coords and init_function values
    xVec[i][0] = i*dx;
    xVec[i][1] = (2.*i+1)/2.*dx;
    xVec[i][2] = (i+1)*dx;
    cellVec[i][0] = initFunction(i*dx);
    cellVec[i][1] = initFunction((2.*i+1)/2.*dx);
    cellVec[i][2] = initFunction((i+1)*dx);
  }
 std::ofstream initout("init.data"); // printing x coords and initial function to a file
 for(int i=0; i<Ncell; i++)
    for(int j=0; j<3; j++) {
     initout << xVec[i][j] << "\t" << cellVec[i][j] << std::endl;
    }
 initout.close();

 // computation of numerical flux INSIDE the domain
 

 //std::vector<std::array<double,3>> rkSteps(Ncell)
 for (int step = 0; step < nt; step++) {
   std::string filename = "solution-" + std::to_string(step) + ".data";
   std::ofstream out(filename); // printing solution to a file
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<3; j++) {
       out << xVec[i][j] << "\t" << cellVec[i][j] << std::endl;
     }
   out.close();
   
   filename = "solution_fine-" + std::to_string(step) + ".data";
   out.open(filename);
   int nInCell = 10;
   double dxInCell = dx/(nInCell-1);
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<nInCell; j++) {
       out << i*dx+j*dxInCell << "\t" << representation(2*(j*dxInCell/dx)-1, cellVec[i]) << std::endl;
     }
   out.close();

   std::vector<std::array<double,3>> k1 = RHS(/*t,*/ cellVec);
   std::vector<std::array<double,3>> k2 = RHS(/*t + dt / 2.0,*/ adding (cellVec, multiplying((dt*0.5),k1)) );
   std::vector<std::array<double,3>> k3 = RHS(/*t + dt / 2.0,*/ adding (cellVec, multiplying((dt*0.5),k2) ));
   std::vector<std::array<double,3>> k4 = RHS(/*t + dt,*/ adding (cellVec, multiplying(dt,k3)));
   
   for(int i=0; i<Ncell; i++)
     for(int j=0; j<3; j++) {
       cellVec[i][j] = cellVec[i][j]+ ((k1[i][j] + 2.0 * k2[i][j] + 2.0 * k3[i][j] + k4[i][j]) * dt / 6.0) ; }
   // for(int i=0; i<Ncell; i++)
   //   for(int j=0; j<3; j++) {
   //     cellVec[i][j] = cellVec[i][j] + dt*k1[i][j];
   //   }
   //std::cout << t << ": " << y << std::endl;
   //t += dt;


 }

 
 return 0;
}
